var gulp = require('gulp'),
    browserSync = require('browser-sync').create();

// Static server:
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './1-presentacion',
            directory: true
        }
    });
});

gulp.task('default', ['browser-sync']);
