### Pruebas

+ Nueva herramienta:
    + Mejor integración.
    + Tarjetas separadas de las normales del proyecto.
    + Ejecuciones por historia.
    [Ejemplo](https://kaptureio.atlassian.net/browse/KI-2474)
+ Conservamos la pruebas de Zephyr durante la migración.

---

+ Ciclos:
    + Nuevas funcionalidades.
        - Definición de la historia de usuario.
        - Creación de pruebas para la historia.
        - Creación de una ejecucición post-desarrollo.
        - Creación de una ejecución para QA.
        - Validación y tests de aceptación.
        - DOD.
    - Ciclos de regresión:
        - Con cada nuevo desarrollo irán aumentando las pruebas y cambiando las prioridades.
        - *Asignación de las ejecuciones*.
        - Kapture Win
        - Kapture io
        - Kapture Quality Control
        - Kapture Alarms
        - Kapture Audits
    - Smoke tests:
        - Con cada nuevo desarrollo irán aumentando las pruebas y cambiando las prioridades.
        - Kapture Win
        - Kapture io
        - Kapture Quality Control
        - Kapture Alarms
        - Kapture Audits
+ Priorización de las pruebas.
    - Orden de ejecución de los tests de regresión.

---

+ Validaciones de nuevas funcionalidades:
    - Todas las pruebas se realizarán en **Internet Explorer 11** como mínimo. Tanto desarrollo como QA.
    + En desarrollo antes de pasar a test. Se genera una ejecución y se comprueba que el desarrollo está conforme a lo especificado.
    + En test antes de pasar a tested. Se genera una ejecución y se comprueba externamente que funciona.
    + En tested antes de pasar a done. Se comprueba que las tareas relacionadas están completadas.
    - Únicamente se revisarán las tarjetas de historia. Si alguna tarjeta de historia no se debe validar estará indicado.

---

+ Automatizaciones
    + Qué pruebas se automatizarán.
    + Set de datos
+ Una prueba por cada error. Futuro.
    + Cubrir los casos que ya nos hemos encontrado.

---

### Bugs

#### Cómo reportamos un error: ###
- ¿Hemos encontrado un error o no estaba bien definido?
    - No funciona
    - Funciona...pero no como se esperaba
- ¿Es reproducible?
    - ¿Pasa siempre?
    - ¿Qué pasos he de seguir para reproducir el error?
    - ¿Podemos adjuntar algo?
        - Captura de pantalla o GIF
        - URL
    - ¿Cuál es el resultado esperado?¿Qué tendría que haber pasado?
- ¿Dónde lo he encontrado?
    - Navegador
    - Tablet
    - Escritorio
- ¿Puedo dar algo de información sobre el navegador o SO?
- Títulos explicativos pero sencillos. Dejemos las descripciones para el campo de descripciones
- Comentario mencionando a la persona o personas interesadas
- Mínima información necesaria a la hora de reportar un error:
    - Pasos completos para reproducir el error.
    - Entorno, plataforma, usuario y URL donde hemos encontrado el error.
    - Descripción del error.
    - Descripción de cómo debería funcionar para que no se considerara un error.
- Si no se ha terminado de definir un bug, se asigna a uno mismo para terminar de completarlo antes de asignarlo a otra persona.
- Los errores reportados que no estén bien definidos se le devolverán al usuario que los haya reportado.

- Textos y literales
    - Validación sin literales
    - Incluirlos en el DOD.
- Novedades del sprint
    - El jueves de test se han de tener preparadas las novedades del sprint.
- =>**A quién lo asignamos**
- =>**Etiquetas**


#### Hotfixes: ####

- Lo primero, deberá estar correctamente reportado.
- Para mejorar la velocidad de respuesta en correcciones en producción se asignarán los hotfixes a una persona determinada.
- Hay que discernir ¿es importate, es urgente o ambas?
- ¿Cuántos usuarios lo están utilizando o pueden utilizarlo actualmente?
- (QA) Cuando se traten **hotfixes** hay que confirmar el número de incidencia para agilizar los trámites.
