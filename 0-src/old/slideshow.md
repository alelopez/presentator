title: Basic Example
author:
  name: Alejandro López
output: slideshow.html
controls: true

--

# Kapture QA

--

### Kapture QA

- Pruebas.
- Bugs.

--

### Pruebas

+ Nueva herramienta.
+ Conservamos la pruebas de Zephyr durante la migración.

--

+ Ciclos.
+ Priorización de las pruebas.

--

+ Proceso de validación de nuevas funcionalidades:
![Proceso de validación](img/20180123_1659_455x482_scrot.png)

--

+ Automatizaciones
+ Test por bug

--

### Bugs

+ Cómo reportamos un error.
+ Hotfixes.

--
