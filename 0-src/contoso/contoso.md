title: Contoso insurance
author:
    name: Alejandro López
    url: https://alelopez.es
output: index.html

--

# Contoso
## Challenge 1

--

### Where do you want to go?

--

### Where do you want to go?

One day, Alice came to a fork in the road and saw a Cheshire cat in a tree.

> _"Which road do I take?" she asked.
His response was a question:
"Where do you want to go?"
"I don't know" Alice answered.
"Then," said the cat, "it doesn't matter."_

--

### The product

- Back office.
- API for distributors.

--

### People involved

 ![](./people_involved.jpg)

- API producers.
- API consumers.
- PO.

--

### The technology

- Architechture.
- Domain-driven development.
- Scalability.

--

### The team

- Internal organization.
- The squads.
- Meassuring the success.

--

### The strategy

Minimum valuable product

--

### The strategy

The planning
![](./roadmap.jpg)

--

### The strategy

Managing change
![](./versioning.jpg)

--

# Contoso
## Challenge 2

--

### Gherkin

```cucumber
Feature: Listing policies for a given customer.
    In order to view the policies for a given customer
    As a API user
    I want to list all the policies the customer has
```

--

### Scenarios

```cucumber
Scenario 1: Non-existing customer.
Scenario 2: Existing customer without policies and no active offer(s).
Scenario 3: Existing customer without policies but with active offer(s).
Scenario 4: Existing customer with active policies.
Scenario 5: Existing customer with no active policies.
```

--

### Mocking before coding

[Postman mock server](https://documenter.getpostman.com/view/842051/Szzq5vbD?version=latest)

--

### Definition of done

- Code is well written.
- Code is checked in.
- Code is peer reviewed.
- Code has all appropiate tests:
    - Unit tets
    - Integration tests
    - E2E
- The feature the code implements has been documented.
